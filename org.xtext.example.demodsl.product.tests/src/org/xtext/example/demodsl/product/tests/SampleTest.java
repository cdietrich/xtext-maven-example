package org.xtext.example.demodsl.product.tests;

import java.io.ByteArrayInputStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.junit.Test;

import com.google.common.base.Charsets;

public class SampleTest {
	
	@Test
	public void testSth() throws CoreException {
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject("demo");
		project.create(new NullProgressMonitor());
		project.open(new NullProgressMonitor());
		IFile file = project.getFile("test.mydsl");
		file.create(new ByteArrayInputStream("Hello World!".getBytes(Charsets.UTF_8)),true, new NullProgressMonitor());
		IDE.openEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), file);
	}

}
