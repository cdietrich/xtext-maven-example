# Xtext Maven Example

This is a Small Example that shows how to Build Xtext with Maven Tycho. Following Features are shown.

- Generating Xtext Languages
- Compiling Xtend Code
- Creating Feature, P2 Repositories and Products
- Running Tests

[License](LICENSE)

![build status](https://gitlab.com/cdietrich/xtext-maven-example/badges/master/build.svg)

<a href="http://with-eclipse.github.io/" target="_blank">
<img alt="with-Eclipse logo" src="http://with-eclipse.github.io/with-eclipse-0.jpg" /></a>